﻿unit MainUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Executor;

type
  TMainForm = class(TForm)
    CodeMemo: TMemo;
    GenerateButton: TButton;
    ConsoleMemo: TMemo;
    ExecuteButton: TButton;
    FreePascalPathEdit: TEdit;
    ResultEdit: TEdit;
    SendResultButton: TButton;
    procedure ExecuteButtonClick(Sender: TObject);
    procedure GenerateButtonClick(Sender: TObject);
    procedure SendResultButtonClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  MainForm: TMainForm;
  ExecThread: TExecutorThread;
  procedure CaptureConsoleOutput(const ACommand, AParameters: String; AMemo: TMemo);

implementation

{$R *.dfm}

{ TMainForm }

procedure CaptureConsoleOutput(const ACommand, AParameters: String; AMemo: TMemo);
 const
   CReadBuffer = 2400;
 var
   saSecurity: TSecurityAttributes;
   hRead: THandle;
   hWrite: THandle;
   suiStartup: TStartupInfo;
   piProcess: TProcessInformation;
   pBuffer: array[0..CReadBuffer] of AnsiChar;
   dRead: DWord;
   Ret: LongBool;
   mOutputs: string;
 begin
   saSecurity.nLength := SizeOf(TSecurityAttributes);
   saSecurity.bInheritHandle := True;
   saSecurity.lpSecurityDescriptor := nil;

   if CreatePipe(hRead, hWrite, @saSecurity, 0) then
   begin
     FillChar(suiStartup, SizeOf(TStartupInfo), #0);
     suiStartup.cb := SizeOf(TStartupInfo);
     suiStartup.hStdInput := hRead;
     suiStartup.hStdOutput := hWrite;
     suiStartup.hStdError := hWrite;
     suiStartup.dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW;
     suiStartup.wShowWindow := SW_HIDE;

     if CreateProcess(nil, PChar(ACommand + ' ' + AParameters), @saSecurity,
       @saSecurity, True, NORMAL_PRIORITY_CLASS, nil, nil, suiStartup, piProcess)
       then
     begin
       CloseHandle(hWrite);
       mOutputs := '';
       repeat
         dRead := 0;
         Ret := ReadFile(hRead, pBuffer[0], CReadBuffer, dRead, nil);
         mOutputs := mOutputs + Copy(pBuffer, 1, dRead);
       until Ret = FALSE;

       CloseHandle(piProcess.hProcess);
       AMemo.lines.Text:=mOutputs;
       CloseHandle(piProcess.hThread);
     end;
     CloseHandle(hRead)

   end;
end;

procedure ExecuteCommand(const ACommand, AParameters: String; AMemo: TMemo);
var
  Security : TSecurityAttributes;
  start : TStartUpInfo;
begin
  AMemo.lines.Clear;

  With Security do
    begin
      nlength := SizeOf(TSecurityAttributes) ;
      binherithandle := true;
      lpsecuritydescriptor := nil;
    end;
  CreatePipe(InputPipeRead, InputPipeWrite, @Security, 0);
  CreatePipe(OutputPipeRead, OutputPipeWrite, @Security, 0);
  CreatePipe(ErrorPipeRead, ErrorPipeWrite, @Security, 0);

  FillChar(Start,Sizeof(Start),#0) ;
  start.cb := SizeOf(start) ;
  start.hStdInput := InputPipeRead;
  start.hStdOutput := OutputPipeWrite;
  start.hStdError :=  ErrorPipeWrite;
  start.dwFlags := STARTF_USESTDHANDLES + STARTF_USESHOWWINDOW;
  start.wShowWindow := SW_HIDE;
  if CreateProcess(nil, PChar(ACommand + ' ' + AParameters), @Security, @Security, true,
             CREATE_NEW_CONSOLE or SYNCHRONIZE,
             nil, nil, start, ProcessInfo) then
  begin
    ExecThread := TExecutorThread.Create(AMemo);
  end;
  MainForm.SendResultButton.Enabled:=true;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  ExecThread.Terminate;
  // close process handles
  CloseHandle(ProcessInfo.hProcess);
  CloseHandle(ProcessInfo.hThread);
  // close pipe handles
  CloseHandle(InputPipeRead);
  CloseHandle(InputPipeWrite);
  CloseHandle(OutputPipeRead);
  CloseHandle(OutputPipeWrite);
  CloseHandle(ErrorPipeRead);
  CloseHandle(ErrorPipeWrite);
end;

procedure TMainForm.GenerateButtonClick(Sender: TObject);
begin
  CodeMemo.Lines.SaveToFile('task.pas');
  CaptureConsoleOutput(FreePascalPathEdit.Text, 'task.pas', ConsoleMemo);
end;

procedure TMainForm.SendResultButtonClick(Sender: TObject);
begin
  WritePipeOut(InputPipeWrite, ResultEdit.Text);
  ConsoleMemo.Lines.Add(ResultEdit.Text);
end;

procedure TMainForm.ExecuteButtonClick(Sender: TObject);
begin
  ExecuteCommand('task', '', ConsoleMemo);
end;


end.
