unit Executor;

interface

uses
  Winapi.Windows, System.Classes, Vcl.StdCtrls, System.SysUtils;

type
  TExecutorThread = class(TThread)
  private
    AMemo: TMemo;

    TextString: String;
    procedure UpdateCaption;

  public
    constructor Create(const AMemo: TMemo);

  protected
    procedure Execute; override;
  end;

var
  InputPipeRead, InputPipeWrite: THandle;
  OutputPipeRead, OutputPipeWrite: THandle;
  ErrorPipeRead, ErrorPipeWrite: THandle;
  ProcessInfo : TProcessInformation;
  ExecThread: TExecutorThread;
  procedure WritePipeOut(OutputPipe: THandle; InString: string);

implementation

procedure WritePipeOut(OutputPipe: THandle; InString: string);
  var
    byteswritten: Cardinal;
    command: AnsiString;
  begin
    command := AnsiString(InString) + #13#10;
    WriteFile(OutputPipe, command[1], Length(command), byteswritten, nil);
  end;

function ReadPipeInput(InputPipe: THandle; var BytesRem: Integer): String;
  var
    TextBuffer: array[1..32767] of Ansichar;
    TextString: String;
    BytesRead: Cardinal;
    PipeSize: Integer;
  begin
    Result := '';
    PipeSize := Sizeof(TextBuffer);

    // �������� ��� � ����� ���-�� ��������
    PeekNamedPipe(InputPipe, nil, PipeSize, @BytesRead, @PipeSize, @BytesRem);
    if bytesread > 0 then
      begin
        ReadFile(InputPipe, TextBuffer, pipesize, bytesread, nil);
        TextString := String(TextBuffer);
        SetLength(TextString, BytesRead);
        Result := TextString;
      end;
  end;

procedure TExecutorThread.Execute;
var
  BytesRem: Integer;
begin
  while not Terminated do
    begin
      // ������ �� ������ ������
      TextString := ReadPipeInput(OutputPipeRead, BytesRem);
      if TextString <> '' then
         Synchronize(UpdateCaption);

      // ������ �� ������ ������
      TextString := ReadPipeInput(ErrorPipeRead, BytesRem);
      if TextString <> '' then
         Synchronize(UpdateCaption);
      sleep(40);
    end;
end;

procedure TExecutorThread.UpdateCaption;
begin
  AMemo.Lines.Add(TextString);
end;

constructor TExecutorThread.Create(const AMemo: TMemo);
begin
  inherited Create(false);
  self.AMemo := AMemo;
  FreeOnTerminate := true;
end;

end.
