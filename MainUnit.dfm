object MainForm: TMainForm
  Left = 238
  Top = 147
  Caption = #1058#1077#1089#1090
  ClientHeight = 401
  ClientWidth = 554
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object CodeMemo: TMemo
    Left = 24
    Top = 17
    Width = 499
    Height = 144
    Lines.Strings = (
      'program Hello;'
      'var'
      '  a,b,x: Integer;'
      'begin'
      '  a:=2;'
      '  b:=3;'
      '  writeln ('#39#1055#1088#1080#1074#1077#1090', '#1052#1080#1088'! '#1057#1082#1086#1083#1100#1082#1086' '#1073#1091#1076#1077#1090' '#39',a,'#39' + '#39',b,'#39'?'#39' );'
      '  x:=a+b;'
      '  read(a, b);'
      '  writeln ('#39#1055#1088#1072#1074#1080#1083#1100#1085#1099#1081' '#1086#1090#1074#1077#1090': '#39',x, '#39' '#1055#1088#1086#1095#1080#1090#1072#1085#1086': '#39', a,'#39' '#1080' '#39',b );'
      'end.')
    TabOrder = 0
  end
  object GenerateButton: TButton
    Left = 349
    Top = 176
    Width = 75
    Height = 25
    Caption = #1043#1077#1085#1077#1088#1072#1094#1080#1103
    TabOrder = 1
    OnClick = GenerateButtonClick
  end
  object ConsoleMemo: TMemo
    Left = 24
    Top = 208
    Width = 492
    Height = 104
    TabOrder = 2
  end
  object ExecuteButton: TButton
    Left = 432
    Top = 176
    Width = 75
    Height = 25
    Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100
    TabOrder = 3
    OnClick = ExecuteButtonClick
  end
  object FreePascalPathEdit: TEdit
    Left = 24
    Top = 177
    Width = 317
    Height = 21
    TabOrder = 4
    Text = 'e:\bin\FPC\3.0.4\bin\i386-win32\fpc.exe'
  end
  object ResultEdit: TEdit
    Left = 24
    Top = 320
    Width = 317
    Height = 21
    TabOrder = 5
    Text = '4'
  end
  object SendResultButton: TButton
    Left = 349
    Top = 319
    Width = 158
    Height = 25
    Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1100' '#1086#1090#1074#1077#1090
    Enabled = False
    TabOrder = 6
    OnClick = SendResultButtonClick
  end
end
