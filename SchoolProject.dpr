program SchoolProject;

uses
  Vcl.Forms,
  MainUnit in 'MainUnit.pas' {MainForm},
  Executor in 'Executor.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
